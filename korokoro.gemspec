
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "korokoro/version"

Gem::Specification.new do |spec|
  spec.name          = "korokoro"
  spec.version       = KoroKoro::VERSION
  spec.authors       = ["Noah Swanson"]
  spec.email         = ["reflex0000@outlook.com"]

  spec.summary       = %q{KoroKoro is a shell written in Ruby}
	spec.homepage      = "https://reflex0000.gq/korokoro"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
end
