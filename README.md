# KoroKoro

A beginner-friendly shell written in Ruby

## Installation

Clone this repository and run `cd korokoro && bundle install`

## Usage

TODO

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

## Contributing

Bug reports and pull requests are welcome on BitBucket at https://bitbucket.org/reflex0000/korokoro.
